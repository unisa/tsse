#2
INSERT{
?x a <https://schema.org/Person> .
}
WHERE{
BIND(spif:buildURI("trackpoi:person2") as ?x)
};

INSERT{
?x a <https://schema.org/Vehicle> .
}
WHERE{
BIND(spif:buildURI("trackpoi:vehicle2") as ?x)
};
#####
#Event Before
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_2_Start') as ?x)
BIND(xsd:time("00:01:00") as ?time) .
};
#1
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_2_End') as ?x)
BIND(xsd:time("00:01:30") as ?time) .
};
INSERT{
?x a time:TemporalEntity .
?x time:hasBeginning trackpoi:person2_isInTheAreaOf_POI_2_Start .
?x time:hasEnd trackpoi:person2_isInTheAreaOf_POI_2_End .
?x trackpoi:eventOf trackpoi:person2 .
?x trackpoi:isInTheAreaOf trackpoi:POI_2 .
}
WHERE{
BIND(spif:buildURI("trackpoi:person2_isInTheAreaOf_POI_2") as ?x)
};
#
#Event Warning
#
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_1_Start') as ?x)
BIND(xsd:time("00:01:30") as ?time) .
};
#1
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_1_End') as ?x)
BIND(xsd:time("00:01:52") as ?time) .
};
INSERT{
?x a time:TemporalEntity .
?x time:hasBeginning trackpoi:person2_isInTheAreaOf_POI_1_Start .
?x time:hasEnd trackpoi:person2_isInTheAreaOf_POI_1_End .
?x trackpoi:eventOf trackpoi:person2 .
?x trackpoi:isInTheAreaOf trackpoi:POI_1 .
}
WHERE{
BIND(spif:buildURI("trackpoi:person2_isInTheAreaOf_POI_1") as ?x)
};
#
#Event Vehicle
#
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:vehicle2_isInTheAreaOf_POI_1_Start') as ?x)
BIND(xsd:time("00:01:22") as ?time) .
};
#1
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:vehicle2_isInTheAreaOf_POI_1_End') as ?x)
BIND(xsd:time("00:01:38") as ?time) .
};
INSERT{
?x a time:TemporalEntity .
?x time:hasBeginning trackpoi:vehicle2_isInTheAreaOf_POI_1_Start .
?x time:hasEnd trackpoi:vehicle2_isInTheAreaOf_POI_1_End .
?x trackpoi:eventOf trackpoi:vehicle2 .
?x trackpoi:isInTheAreaOf trackpoi:POI_1 .
}
WHERE{
BIND(spif:buildURI("trackpoi:vehicle2_isInTheAreaOf_POI_1") as ?x)
};

INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_2_Start2') as ?x)
BIND(xsd:time("00:01:52") as ?time) .
};
#1
INSERT{
?x a time:Instant .
?x time:inXSDTime ?time .
}
WHERE{
BIND(spif:buildURI('trackpoi:person2_isInTheAreaOf_POI_2_End2') as ?x)
BIND(xsd:time("00:02:30") as ?time) .
};
INSERT{
?x a time:TemporalEntity .
?x time:hasBeginning trackpoi:person2_isInTheAreaOf_POI_2_Start2 .
?x time:hasEnd trackpoi:person2_isInTheAreaOf_POI_2_End2 .
?x trackpoi:eventOf trackpoi:person2 .
?x trackpoi:isInTheAreaOf trackpoi:POI_2 .
}
WHERE{
BIND(spif:buildURI("trackpoi:person2_isInTheAreaOf_POI_2_2") as ?x)
};



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.topbraid.spin.system.SPINModuleRegistry;
import org.topbraid.spin.util.JenaUtil;

/**
 * Demonstrates how to efficiently use an external SPIN library, such as OWL RL
 * to run inferences on a given Jena model.
 * 
 * The main trick is that the Query maps are constructed beforehand, so that the
 * actual query model does not need to include the OWL RL model at execution
 * time.
 * 
 * @author Holger Knublauch
 */
public class OWLRLExample {
	static String PREFIX = "PREFIX trackpoi:<http://www.semanticweb.org/danilo/ontologies/2016/1/TrackPOI#> \n"
			+ "PREFIX fn:<http://topbraid.org/functions-fn#> \n" + "PREFIX spif:<http://spinrdf.org/spif#> \n"
			+ "PREFIX time:<http://www.w3.org/2006/time#>\n PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> \n";

	public static void main(String[] args) {
		String build;
		Query query;
		QueryExecution qe;
		ResultSet results;

		// Initialize system functions and templates
		SPINModuleRegistry.get().init();

		String SOURCE = "file:///Demo/TrackPOI_Esempi_costruiti.rdf";

		System.out.println("Loading domain ontology...");
		// OntModel queryModel = loadModelWithImports(SOURCE);

		OntModel queryModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		queryModel.read("queryDEMO/TrackPOI_Esempi_costruiti.rdf", "RDF/XML");

		// Load OWL RL library from the web
		System.out.println("Loading OWL RL ontology...");
		OntModel owlrlModel = loadModelWithImports("http://topbraid.org/spin/owlrl-all");

		// Register any new functions defined in OWL RL
		SPINModuleRegistry.get().registerAll(owlrlModel, null);

		// Leggo input per la finestra temporale
		Scanner s = new Scanner(System.in);
		int window, t1, t2;
		do {
			System.out.print("Inserisci lunghezza finestra (sec):");
			window = s.nextInt();
		} while (window < 0);

		// Mi calcolo l'ultimo istante (fine ciclo) -----
		build = PREFIX + "SELECT ?x WHERE{ ?time time:inXSDTime ?x .?time a time:Instant . } ORDER BY DESC(?x) LIMIT 1";
		query = QueryFactory.create(build);
		qe = QueryExecutionFactory.create(query, queryModel);
		results = qe.execSelect();
		// ResultSetFormatter.out(System.out, results, query);
		QuerySolution solN = results.nextSolution();
		String[] element = solN.toString().split("\"");

		long max;
		try {
			max = date2sec(element[1]);
		} catch (ParseException e) {
			max = 0;
		}
		//System.out.println(sec2Date(max));
		// --------------------------------------------

		// Parto dall'istante 00:00:00 e faccio uno shift di x secondi per ogni
		// query
		t1 = 0;
		t2 = window;
		s.nextLine();// serve x pulire lo stream
		 while (t1 < max){
			System.out.println("Finestra(" + sec2Date(t1) + "<--->" + sec2Date(t2) + ")" );
			build = buildQuery(t1, t2);
			query = QueryFactory.create(build);
			qe = QueryExecutionFactory.create(query, queryModel);
			results = qe.execSelect();
			ResultSetFormatter.out(System.out, results, query);
			System.out.println("Press any key to continue...");
			
			s.nextLine();
			t1 = t2;
			t2 = t1 + window;
		}
	}

	private static long date2sec(String string) throws ParseException {
		DateFormat format = new SimpleDateFormat("HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date date = format.parse(string);
		return date.getTime() / 1000;
	}

	private static String buildQuery(int t1, int t2) {
		String build = PREFIX + "SELECT DISTINCT ?eventDang ?eventBefore ?eventVeicolo ?begT  ?endT " + "WHERE{"
				+ "BIND(trackpoi:POI_1 as ?strada) . BIND(xsd:time(\"" + sec2Date(t1)
				+ "\") as ?t1 ) . BIND(xsd:time(\"" + sec2Date(t2) + "\") as ?t2 ) ."
				+ "?eventBefore a time:TemporalEntity ." + "?eventBefore trackpoi:eventOf ?obj1."
				+ "MINUS {?eventBefore trackpoi:isInTheAreaOf trackpoi:POI_1 .}"
				+ "?eventBefore time:intervalMeets ?eventDang . ?eventDang trackpoi:eventOf ?obj1 ."
				+ "?obj1 a <https://schema.org/Person> . ?eventDang trackpoi:isInTheAreaOf ?strada ."
				+ "{?eventDang time:intervalDuring ?eventVeicolo . } UNION"
				+ "{?eventDang time:intervalOverlaps ?eventVeicolo . } UNION"
				+ "{?eventDang time:intervalOverlappedBy ?eventVeicolo . } UNION"
				+ "{?eventDang time:intervalContains ?eventVeicolo . } ?eventVeicolo  trackpoi:eventOf ?obj2  ."
				+ "?obj2 a <https://schema.org/Vehicle> . ?eventVeicolo  trackpoi:isInTheAreaOf ?strada ."
				+ "?eventVeicolo time:hasBeginning ?beg . ?eventVeicolo time:hasEnd ?end ."
				+ "?beg time:inXSDTime ?begT . ?end time:inXSDTime ?endT . "
				+ "FILTER(?begT>?t1) . FILTER(?endT<?t2) . }";
		return build;
	}

	private static String sec2Date(long max) {
		Date date = new Date(max * 1000L);
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
		df2.setTimeZone(TimeZone.getTimeZone("GMT"));
		return df2.format(date);

	}

	private static OntModel loadModelWithImports(String url) {
		Model baseModel = ModelFactory.createDefaultModel();
		baseModel.read(url);
		return JenaUtil.createOntologyModel(OntModelSpec.OWL_MEM, baseModel);
	}
}
